#!/system/bin/sh
# Please don't hardcode /magisk/modname/... ; instead, please use $MODDIR/...
# This will make your scripts compatible even if Magisk change its mount point in the future
MODDIR=${0%/*}

MODNAME="los-switch-hw-buttons"

log_print() {
  echo "$MODNAME: $1"
  #echo "$MODNAME: $1" >> /cache/los-switch-buttons.log
  #log -p i -t "$MODNAME" "$1"
}

log_print ""
log_print "STARTUP"

# This script will be executed in post-fs-data mode
# More info in the main Magisk thread

DIR=/system/usr/keylayout

if [ ! -f ${DIR}/synaptics_dsx.kl ]; then
    # Try Treble
    DIR=/vendor/usr/keylayout
    MODDIR=${MODDIR}/system
fi

if [ ! -f ${DIR}/synaptics_dsx.kl ]; then
	log_print "ERROR: Could not find keylayout dir"
fi

FILE=${MODDIR}${DIR}/synaptics_dsx.kl

log_print `cp -v "${DIR}/synaptics_dsx.kl" "${FILE}"`

APP_SWITCH=$(cat "${FILE}" | awk '$1 == "key" && $3 == "APP_SWITCH" {print $2}')
BACK=$(cat "${FILE}" | awk '$1 == "key" && $3 == "BACK" {print $2}')            

log_print "APP_SWITCH: $APP_SWITCH"
log_print "BACK: $BACK"

sed -i "s/key[ \t]*${APP_SWITCH}[ \t]*APP_SWITCH/key ${BACK}    APP_SWITCH/g" "${FILE}"
sed -i "s/key[ \t]*${BACK}[ \t]*BACK/key ${APP_SWITCH}    BACK/g" "${FILE}"